import cvxopt
import numpy as np
import predictor as pr
import kernel as kl

"""Minimalny uwzgledzniany wspolczynnik Lagrangea"""
MIN_SUPPORT_VECTOR_MULTIPLIER = 1e-6


class Trainer(object):
    def __init__(self, kernel, max_c=10):
        self._kernel = kernel
        self._c = max_c

    def train(self, X, y):
        """Given the training features X with labels y, returns a SVM
        predictor representing the trained SVM.
        """
        lagrange_multipliers = self._compute_multipliers(X, y)

        # Boolean array shows which multiplier is valid (big enough)
        support_vector_indices = lagrange_multipliers > MIN_SUPPORT_VECTOR_MULTIPLIER

        # Masked lagrange multipliers
        support_multipliers = lagrange_multipliers[support_vector_indices]

        # Masked support vectors choosen from input set
        support_vectors = X[support_vector_indices]

        # Masked group identifiers (connected with support_vectors)
        support_vector_labels = y[support_vector_indices]

        # MASKI DLA TABLICY WARTOSCIOWOSCI
        support_vector_indices_1 = np.squeeze(np.asarray(support_vector_labels == 1.0))
        support_vector_indices_not1 = np.squeeze(np.asarray((support_vector_labels == -1.0)))

        # support_vectors_1 = support_vectors[]
        #
        support_vector1 = (support_vectors[support_vector_indices_1])[0]
        support_vector2 = (support_vectors[support_vector_indices_not1])[0]

        nbr_of_samples, nbr_of_dimens = support_vectors.shape

        # Calculating w-vector
        w = np.zeros(nbr_of_dimens)
        for i in range(nbr_of_dimens):
            for j in range(nbr_of_samples):
                w[i] += support_multipliers[j] * support_vector_labels[j] * np.array(support_vectors)[j][i]

        # Calculating b-coef.
        b = np.array([1., -1.])

        for i in range(nbr_of_dimens):
            b[0] -= w[i] * np.array(support_vector1)[0][i]
            b[1] -= w[i] * np.array(support_vector2)[0][i]

        b = (b[0] + b[1]) / 2

        # Predictor creation
        return pr.Predictor(w, b, self._kernel)

    def _gram_matrix(self, X):
        n_samples, n_features = X.shape
        K = np.zeros((n_samples, n_samples))

        for i, x_i in enumerate(X):
            for j, x_j in enumerate(X):
                K[i, j] = self._kernel(x_i, x_j)
        return K

    def _compute_multipliers(self, X, y):
        n_samples, n_features = X.shape

        K = self._gram_matrix(X)
        # Solves
        # min 1/2 x^T P x + q^T x
        # s.t.
        #  Gx \coneleq h
        #  Ax = b

        P = cvxopt.matrix(np.outer(y, y) * K)
        q = cvxopt.matrix(-1 * np.ones(n_samples))

        # -a_i \leq 0
        # TODO(tulloch) - modify G, h so that we have a soft-margin classifier
        G_std = cvxopt.matrix(np.diag(np.ones(n_samples) * -1))
        h_std = cvxopt.matrix(np.zeros(n_samples))

        # a_i \leq c
        G_slack = cvxopt.matrix(np.diag(np.ones(n_samples)))
        h_slack = cvxopt.matrix(np.ones(n_samples) * self._c)

        G = cvxopt.matrix(np.vstack((G_std, G_slack)))
        h = cvxopt.matrix(np.vstack((h_std, h_slack)))

        A = cvxopt.matrix(y, (1, n_samples))
        b = cvxopt.matrix(0.0)

        solution = cvxopt.solvers.qp(P, q, G, h, A, b)

        # Lagrange multipliers
        return np.ravel(solution['x'])


if __name__ == "__main__":

    X = np.matrix([[0.13856, 0.5131],
                   [0.25921, 0.26108],
                   [0.90102, 0.58198],
                   [0.02212, 0.7043],
                   [0.2488, 0.9904],
                   [0.22374, 0.56751],
                   [0.86701, 0.18306],
                   [0.00865, 0.45062],
                   [0.04829, 0.68082],
                   [0.24042, 0.8313],
                   [1.09778, 1.235],
                   [1.02459, 1.08723],
                   [1.50164, 1.06381],
                   [1.07053, 1.14818],
                   [1.70556, 1.78613],
                   [1.0746, 1.99822],
                   [1.7923, 1.44644],
                   [1.68212, 1.85668],
                   [1.34193, 1.08593],
                   [1.37754, 1.95356]])

    y = np.matrix([[1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [-1],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.]])

    trainer = Trainer(kl.Kernel.linear(), 10)
    predictor = trainer.train(X, y)

    x = np.matrix([[1., 1.5],
                   [0., 0.]])

    true_result = np.matrix([[-1.0],
                             [1.0]])

    if np.array_equal(predictor.predict(x), true_result):
        print "OK!"
    else:
        print "Error!"
    #
    # import matplotlib.pyplot as plt
    #
    # plt.scatter(*zip(*np.array(X)))
    #
    # W = np.array([[1.0, 1.5]])
    # plt.scatter(*zip(*W), color='red')
    # # plt.show()
    #
    #
    # # PRZECHODZIMY DO WERSJI   Y=AX+B
    # # HARDCODING
    # w = predictor._w
    # b = predictor._b
    #
    # print(w[0])
    # print(b)
    #
    # a = -1.0 * w[0] / w[1]
    # b = -1.0 * b / w[1]
    #
    # x = np.linspace(0.0, 2.0, num=100)
    # y = a * x + b
    #
    # plt.scatter(x, y, color='red')
    #
    # test_point = np.array([1., 1.5])
    # test_point = np.dot(test_point, w) + b
    # wynik = np.sign(test_point)
    #
    # print(wynik)
    # print(type(wynik))
    #
    # plt.show()
