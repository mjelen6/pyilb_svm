import numpy as np
import kernel as kl


class Predictor(object):
    def __init__(self, w, b, kernel):
        self._w = np.matrix(w)
        self._b = np.matrix(b)
        self._kernel = kernel

    def _check_one_vect(self, x):
        return np.sign(self._kernel(self._w,x) + self._b)

    def predict(self, x):
        """
        Computes the SVM prediction on the given features x.
        """
        inp = np.matrix(x)
        nbr_of_samples, nbr_of_dimensions = inp.shape

        out = np.matrix(np.zeros((nbr_of_samples, 1)))

        for i in range(nbr_of_samples):
            out[i] = self._check_one_vect(inp[i])

        return out


if __name__ == "__main__":

    w = np.matrix([[-2.14956352, -3.43271262]])
    b = np.matrix([[4.93456962177]])
    x = np.matrix([[1., 1.5],
                   [0., 0.]])

    true_result = np.matrix([[-1.0],
                             [1.0]])

    pred = Predictor(w, b, kl.Kernel.linear())

    if np.array_equal(pred.predict(x), true_result):
        print "OK!"
    else:
        print "Error!"
