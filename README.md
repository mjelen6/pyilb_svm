# Pyhon and Linux Bash: Support Vector Machine #


## Repository Description ##
This repository contains our implementation of Support Vector Machine algorithm, which was done as a final project for one of our subjects: "Programming in scripting lanuages: Python and Linux Bash".

## Stack and development tools ##
-python 2.7
-virtualenv
-pip
-pyscaffold

## Authors ##
See AUTHORS.rst

## Version ##
See CHANGES.rst or use git repo to see changes.

## License ##
See LICENSE.txt

# Setup #
Project uses template created by pyscaffold. The easiest way of development or use is to clone git repository and prepare virtual environment for python. You may also operate on your system default python version and packages, but in this case you will have to change it or install additional packages what might result in conflicts between your python projects.
## for development or tests ##
## for use ##